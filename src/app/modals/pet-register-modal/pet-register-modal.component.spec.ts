import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetRegisterModalComponent } from './pet-register-modal.component';

describe('PetRegisterModalComponent', () => {
  let component: PetRegisterModalComponent;
  let fixture: ComponentFixture<PetRegisterModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetRegisterModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetRegisterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
