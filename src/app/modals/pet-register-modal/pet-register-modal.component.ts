import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup } from '@angular/forms';
import { PetsService } from 'src/app/services/pets.service'; 

@Component({
  selector: 'app-pet-register-modal',
  templateUrl: './pet-register-modal.component.html',
  styleUrls: ['./pet-register-modal.component.css']
})
export class PetRegisterModalComponent implements OnInit {

  @Input() petId: any;
  public pet: any;

  form = new FormGroup({
    owner_name: new FormControl(),
    owner_last_name: new FormControl(),
    pet_name: new FormControl(),
    pet_last_name: new FormControl(),
    species: new FormControl(),
    breed: new FormControl(),
    age: new FormControl()
  })

  constructor(public activeModal: NgbActiveModal, private petsService: PetsService) {}

  ngOnInit(): void {
    if(this.petId){
      this.getPet(this.petId);
    }
  }

  /**
   * this method create a new pet and close the modal
   */
  onSubmit() {
    this.petsService.createPet(this.form.value).subscribe((data: any) => {
      this.activeModal.close();
      alert('Pet successfully created')
    }, (error => {
      console.log('An error has occurred')
    }));
  }

   /**
   * this method create a new pet and close the modal
   */
   getPet(petId: number) {
    this.petsService.getPet(petId).subscribe((pet: any) => {
      this.pet = pet;
      this.form.patchValue(pet);
    }, (error => {
      console.log('An error has occurred')
    }));
  }

  updatePet() {
    this.petsService.updatePet(this.petId.toString(), this.form.value).subscribe((pet: any) => {
      this.activeModal.close();
      alert('pet updated successfully');
    }, (error => {
      console.log('An error has occurred')
    }));
  }

  closeModal() {
    this.activeModal.close('Close click');
  }

}
