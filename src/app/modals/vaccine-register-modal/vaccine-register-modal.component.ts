import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup } from '@angular/forms';
import { VaccinesService } from 'src/app/services/vaccines.service';
import { PetsService } from 'src/app/services/pets.service';

@Component({
  selector: 'app-vaccine-register-modal',
  templateUrl: './vaccine-register-modal.component.html',
  styleUrls: ['./vaccine-register-modal.component.css']
})
export class VaccineRegisterModalComponent implements OnInit {

  @Input() vaccineId: any;
  public vaccine: any;

  form = new FormGroup({
    name: new FormControl(),
    description: new FormControl(),
    application_date: new FormControl(),
    next_date: new FormControl(),
    vet: new FormControl(),
    notes: new FormControl(),
    pet: new FormControl()
  })

  pets: any[] = [];
  petId: string = "";

  constructor(
    public activeModal: NgbActiveModal,
    private vaccinesService: VaccinesService,
    private petsService: PetsService
  ) {}

  ngOnInit(): void {
    this.getPets();
    if(this.vaccineId){
      this.getVaccine(this.vaccineId.toString());
    }
  }

  /**
   * this method gets all registred pets
   */
  getPets(): void {
    this.petsService.getPets().subscribe(
      pets => {
        this.pets = pets.data;
        console.log('Pets obtained correctly', this.pets);
      },
      error => {
        console.error('Error getting pets', error);
      }
    );
  }

  /**
   * this method create a new vaccine and close the modal
   */
  onSubmit() {
    this.vaccinesService.createVaccine(this.form.value).subscribe((data: any) => {
      this.activeModal.close();
      alert('Vaccine successfully created')
    }, (error => {
      console.log('An error has occurred')
    }));
  }

  /**
   * this method create a new vaccine and close the modal
   */
  getVaccine(vaccineId: number) {
    this.vaccinesService.getVaccine(vaccineId).subscribe((vaccine: any) => {
      this.vaccine = vaccine;
      this.form.patchValue(vaccine);
    }, (error => {
      console.log('An error has occurred')
    }));
  }

  updateVaccine() {
    this.form.value.pet = this.form.value.pet.toString()
    this.vaccinesService.updateVaccine(this.vaccineId.toString(), this.form.value).subscribe((vaccine: any) => {
      this.activeModal.close();
      alert('vaccine updated successfully');
    }, (error => {
      console.log('An error has occurred')
    }));
  }


  closeModal() {
    this.activeModal.close('Close click');
  }

}
