import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaccineRegisterModalComponent } from './vaccine-register-modal.component';

describe('VaccineRegisterModalComponent', () => {
  let component: VaccineRegisterModalComponent;
  let fixture: ComponentFixture<VaccineRegisterModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaccineRegisterModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccineRegisterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
