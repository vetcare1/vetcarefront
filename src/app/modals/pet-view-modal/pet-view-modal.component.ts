import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PetsService } from 'src/app/services/pets.service';

@Component({
  selector: 'app-pet-view-modal',
  templateUrl: './pet-view-modal.component.html',
  styleUrls: ['./pet-view-modal.component.css']
})
export class PetViewModalComponent implements OnInit {

  @Input() petId: any;
  public pet: any;

  constructor(
    public activeModal: NgbActiveModal,
    private petsService: PetsService
  ) { }

  ngOnInit(): void {
    this.getPet(this.petId);
  }

  /**
   * this method retrieves a pet information by id
   */
  public getPet(petId: number) {
    this.petsService.getPet(petId).subscribe((pet: any) => {
      this.pet = pet
    })
  }
  
  closeModal() {
    this.activeModal.close('Close click');
  }

}
