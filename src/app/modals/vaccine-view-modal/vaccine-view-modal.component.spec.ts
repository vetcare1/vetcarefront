import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaccineViewModalComponent } from './vaccine-view-modal.component';

describe('VaccineViewModalComponent', () => {
  let component: VaccineViewModalComponent;
  let fixture: ComponentFixture<VaccineViewModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaccineViewModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccineViewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
