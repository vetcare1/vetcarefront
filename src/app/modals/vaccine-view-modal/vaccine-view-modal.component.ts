import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PetsService } from 'src/app/services/pets.service';
import { VaccinesService } from 'src/app/services/vaccines.service';

@Component({
  selector: 'app-vaccine-view-modal',
  templateUrl: './vaccine-view-modal.component.html',
  styleUrls: ['./vaccine-view-modal.component.css']
})
export class VaccineViewModalComponent implements OnInit {

  @Input() vaccineId: any;
  public vaccine: any;
  public pet: any;

  constructor(
    public activeModal: NgbActiveModal,
    private vaccineService: VaccinesService,
    private petsService: PetsService
  ) { }

  ngOnInit(): void {
    this.getVaccine(this.vaccineId);
  }

  /**
   * this method retrieves a vaccine information by id
   */
  public getVaccine(vaccineId: number) {
    this.vaccineService.getVaccine(vaccineId).subscribe((vaccine: any) => {
      this.vaccine = vaccine
      this.petsService.getPet(vaccine.id).subscribe((pet: any) => {
        this.pet = pet
      })
    })
  }

  closeModal() {
    this.activeModal.close('Close click');
  }
}
