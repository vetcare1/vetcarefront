import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { PetsComponent } from './components/pets/pets.component';
import { VaccinesComponent } from './components/vaccines/vaccines.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PetRegisterModalComponent } from './modals/pet-register-modal/pet-register-modal.component';
import { VaccineRegisterModalComponent } from './modals/vaccine-register-modal/vaccine-register-modal.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { PetViewModalComponent } from './modals/pet-view-modal/pet-view-modal.component';
import { VaccineViewModalComponent } from './modals/vaccine-view-modal/vaccine-view-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PetsComponent,
    VaccinesComponent,
    PetRegisterModalComponent,
    VaccineRegisterModalComponent,
    PetViewModalComponent,
    VaccineViewModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
