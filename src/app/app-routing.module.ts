import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PetsComponent } from './components/pets/pets.component';
import { VaccinesComponent } from './components/vaccines/vaccines.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'pets', component: PetsComponent
  },
  {
    path: 'vaccines', component: VaccinesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
