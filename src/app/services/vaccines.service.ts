import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VaccinesService {

  constructor(protected http: HttpClient) { }

  /**
   * [URL]
   * @description request endpoint
   * @protected
   * @memberof VaccinesService
   */
  protected URL = `${env.API}vaccine`

  /**
   * [getVaccines]
   * @description Service that get the information for all vaccines
   * @memberof VaccinesService
   */
  getVaccines(params?: HttpParams): Observable<any> {
    return this.http.get<any>(this.URL, { params });
  }

  /**
   * [getVaccine]
   * @description Service that get the information for an specific vaccine
   * @memberof VaccinesService
   */
  getVaccine(vaccineId: number): Observable<any> {
    return this.http.get<any>(`${this.URL}/${vaccineId}`);
  }

  /**
   * [createVaccine]
   * @description Service that create a new Vaccine
   * @memberof VaccinesService
   */
  createVaccine(body: any): Observable<any> {
    return this.http.post(this.URL, body);
  }

  /**
   * [updateVaccine]
   * @description Service that update a new Vaccine
   * @memberof VaccinesService
   */
  updateVaccine(vaccineId: string, body: any): Observable<any> {
    return this.http.put(`${this.URL}/${vaccineId}`, body);
  }

  /**
   * [deleteVaccine]
   * @description Service that delete a Vaccine
   * @memberof VaccineService
   */
  deleteVaccine(vaccineId: number): Observable<any> {
    return this.http.delete(`${this.URL}/${vaccineId}`);
  }
}
