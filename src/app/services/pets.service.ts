import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PetsService {

  constructor(protected http: HttpClient) { }

  /**
   * [URL]
   * @description request endpoint
   * @protected
   * @memberof PetsService
   */
  protected URL = `${env.API}pet`

  /**
   * [getPets]
   * @description Service that get the information for all pets
   * @memberof PetsService
   */
  getPets(params?: HttpParams): Observable<any> {
    return this.http.get<any>(this.URL, { params });
  }

  /**
   * [getPet]
   * @description Service that get the information for an specific pet
   * @memberof PetsService
   */
  getPet(petId: number): Observable<any> {
    return this.http.get<any>(`${this.URL}/${petId}`);
  }

  /**
   * [createPet]
   * @description Service that create a new Pet
   * @memberof PetsService
   */
  createPet(body: any): Observable<any> {
    return this.http.post(this.URL, body);
  }

  /**
   * [updatePet]
   * @description Service that update a new Pet
   * @memberof PetsService
   */
  updatePet(petId: string, body: any): Observable<any> {
    return this.http.put(`${this.URL}/${petId}`, body);
  }

  /**
   * [deletePet]
   * @description Service that delete a Pet
   * @memberof PetService
   */
  deletePet(petId: number): Observable<any> {
    return this.http.delete(`${this.URL}/${petId}`);
  }
}
