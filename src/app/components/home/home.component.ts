import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { PetRegisterModalComponent } from '../../modals/pet-register-modal/pet-register-modal.component'
import { VaccineRegisterModalComponent } from '../../modals/vaccine-register-modal/vaccine-register-modal.component'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'appBootstrap';
  
  closeResult: string = '';
  constructor(private modalService: NgbModal) { }

  openPetRegisterModal() {
    const modalRef = this.modalService.open(PetRegisterModalComponent, {
      centered: true,
      size: "lg",
    });
  }

  openVaccineRegisterModal() {
    const modalRef = this.modalService.open(VaccineRegisterModalComponent, {
      centered: true,
      size: "lg",
    });
  }

  ngOnInit(): void {
  }

}
