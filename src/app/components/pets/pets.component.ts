import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PetRegisterModalComponent } from 'src/app/modals/pet-register-modal/pet-register-modal.component';
import { PetViewModalComponent } from 'src/app/modals/pet-view-modal/pet-view-modal.component';
import { PetsService } from 'src/app/services/pets.service';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css']
})
export class PetsComponent implements OnInit {

  pets: any[] = [];

  constructor(private petsService: PetsService,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getPets();
  }

  /**
   * this method gets all pets
   */
  getPets(): void {
    this.petsService.getPets().subscribe(
      pets => {
        this.pets = pets.data;
        console.log('Pets obtained correctly', this.pets);
      },
      error => {
        console.error('Error getting pets', error);
      }
    );
  }

  openPetViewModal(petId: number) {
    const modalRef = this.modalService.open(PetViewModalComponent, {
      centered: true,
      size: "lg",
    });
    modalRef.componentInstance.petId = petId;
  }

  /**
   * this method delete an specific pet information
   * @param {string} petId pet id to delete
   */
  public deletePet(petId: number) {
    this.petsService.deletePet(petId).subscribe((pet: any) => {
      this.getPets();
    })
  }

  openPetEditModal(petId: number) {
    const modalRef = this.modalService.open(PetRegisterModalComponent, {
      centered: true,
      size: "lg",
    });
    modalRef.componentInstance.petId = petId;
  }

}
