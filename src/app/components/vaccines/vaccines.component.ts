import { Component, OnInit } from '@angular/core';
import { VaccinesService } from '../../services/vaccines.service'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VaccineViewModalComponent } from 'src/app/modals/vaccine-view-modal/vaccine-view-modal.component';
import { VaccineRegisterModalComponent } from 'src/app/modals/vaccine-register-modal/vaccine-register-modal.component';

@Component({
  selector: 'app-vaccines',
  templateUrl: './vaccines.component.html',
  styleUrls: ['./vaccines.component.css']
})
export class VaccinesComponent implements OnInit {
  vaccines: any[] = [];

  constructor(private vaccinesService: VaccinesService,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getVaccines();
  }

  /**
   * this method gets all vaccines
   */
  getVaccines(): void {
    this.vaccinesService.getVaccines().subscribe(
      vaccines => {
        this.vaccines = vaccines.data;
        console.log('Vaccines obtained correctly', this.vaccines);
      },
      error => {
        console.error('Error getting vaccines', error);
      }
    );
  }

  openVaccineViewModal(vaccineId: number) {
    const modalRef = this.modalService.open(VaccineViewModalComponent, {
      centered: true,
      size: "lg",
    });
    modalRef.componentInstance.vaccineId = vaccineId;
  }

  /**
   * this method delete an specific vaccine information
   * @param {string} vaccineId vaccine id to delete
   */
  public deleteVaccine(petId: number) {
    this.vaccinesService.deleteVaccine(petId).subscribe((vaccine: any) => {
      this.getVaccines();
    })
  }

  openVaccineEditModal(vaccineId: number) {
    const modalRef = this.modalService.open(VaccineRegisterModalComponent, {
      centered: true,
      size: "lg",
    });
    modalRef.componentInstance.vaccineId = vaccineId;
  }
}
